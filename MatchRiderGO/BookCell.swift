
import UIKit

class BookCell: UITableViewCell {

  static let identifier = "BookCell"
  
  @IBOutlet weak var driverName: UILabel!
  @IBOutlet weak var ratingView: Rating!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var driverView: IconView!
  @IBOutlet weak var bookButton: UIButton!
  
  func setStateWithDriverName(_ name: String, rating: Int, date: Date, image: UIImage?, price: Int) {
    driverName.text = name
    ratingView.setCount(rating)
    dateLabel.text = getDateWithDay(date,timeFlag: true)
    let price = CentsToString(price)
    bookButton.setTitle(price, for: UIControlState())
    if let image = image {
      driverView.image = image
    }
  }

}
