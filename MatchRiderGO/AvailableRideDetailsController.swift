
import UIKit

class AvailableRideDetailsController: DetailsController {
  
  @IBOutlet var descriptionLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    descriptionLabel.text = ride.driver?.description
  }
  
  @IBAction func bookRide() {
    bookRide(ride)
  }
  
}
