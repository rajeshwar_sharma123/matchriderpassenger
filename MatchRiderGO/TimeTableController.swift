
import UIKit

class TimeTableController: UITableViewController {

  var route: Route!
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.isToolbarHidden = true
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = RouteDirectionString(route.fromShortName, to: route.toShortName)
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return route.matchpoints.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let matchpoint = route.matchpoints[(indexPath as NSIndexPath).row]
    let cell = tableView.dequeueReusableCell(withIdentifier: TimeTableCell.identifier, for: indexPath) as! TimeTableCell
    let minutes = matchpoint.delay
    let name = matchpoint.info
    let position = positionForRow((indexPath as NSIndexPath).row, count: route.matchpoints.count)
    cell.setState(minutes, matchpoint: name, position: position)
    return cell
  }
  
  @IBAction func cancel() {
    self.dismiss(animated: true, completion: nil)
  }
  
}

private func positionForRow(_ row: Int, count: Int) -> TimeTableCell.Position {
  switch row {
  case 0:
    return .First
  case count - 1:
    return .Last
  default:
    return .Between
  }
}
