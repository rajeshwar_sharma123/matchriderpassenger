
import UIKit
import Stripe
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class BankAccountController: UITableViewController {
  
  @IBOutlet var ibanField: VSTextField!
  @IBOutlet var nameField: UITextField!
  @IBOutlet var addressField: UITextField!
  @IBOutlet var cityField: UITextField!
  @IBOutlet var zipField: UITextField!
  @IBOutlet var licenceCell: UITableViewCell!
  @IBOutlet var agreeButton: UIButton!
  @IBOutlet var deleteButton: UIButton!
  
  @IBOutlet var checkImage: UIImageView!
  
  var licenceChecked = false
  var ibanString = ""
    
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    guard let token = Store.sharedStore.token else {
      return
    }
    updateButtons()
    enableAllButton(false)
    HttpRequest.sharedRequest.bankAccount(token) { account in
      guard let account = account else {
        self.enableAllButton(true)
        self.updateButtons()
        self.deleteButton.userFeedbackEnabled(false)
        return
      }
        
      self.ibanField.text = account.last4
      self.nameField.text = account.name
      self.addressField.text = account.address
      self.cityField.text = account.city
      self.zipField.text = account.zip
      self.deleteButton.userFeedbackEnabled(true)
      
    }
    self.ibanField.setFormatting("#### #### #### #### #### ##", replacementChar: "#")
    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
  }
  
  @IBAction func deleteAccount() {
    guard let token = Store.sharedStore.token else {
      return
    }
    HttpRequest.sharedRequest.deleteBankAccount(token) { success in
        guard success else {
            return
        }
        self.enableAllButton(true)
        self.ibanField.text = nil
        self.nameField.text = nil
        self.addressField.text = nil
        self.cityField.text = nil
        self.zipField.text = nil
        self.updateButtons()
        self.deleteButton.userFeedbackEnabled(false)
    }
  }
  
  @IBAction func updateButtons() {
    let ibanSet = ibanField.text?.characters.count >= 4
    let nameSet = nameField.text != ""
    let addressSet = addressField.text != ""
    let citySet = cityField.text != ""
    let zipSet = zipField.text != ""
    agreeButton.userFeedbackEnabled(ibanSet && nameSet && addressSet && citySet && zipSet && licenceChecked)
    checkImage.image = CheckedImage(licenceChecked)
  }
 
  @IBAction func addAccount() {
    guard let token = Store.sharedStore.token else {
      return
    }
    enableAllButton(false)
    
    let sourceParams = STPSourceParams.sepaDebitParams(withName: nameField.text!, iban: ibanField.text!, addressLine1: addressField.text!, city: cityField.text!, postalCode: zipField.text!, country: "DE")
        
    HttpRequest.sharedRequest.createBankAccount(token, sourceParams: sourceParams) { success in
      self.enableAllButton(true)
      let controller = ActionControllerBankAccount(success) {
        if success {
          self.dismiss(animated: true, completion: nil)
        }
      }
      self.present(controller, animated: true, completion: nil)
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let navi = segue.destination as! UINavigationController
    let target = navi.topViewController as! LicenceController
    target.legalPath = "StripeLegal"
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard tableView.cellForRow(at: indexPath) == licenceCell else {
      return
    }
    licenceChecked = !licenceChecked
    updateButtons()
  }
  
  fileprivate func enableAllButton(_ enable: Bool) {
    ibanField.userFeedbackEnabled(enable)
    nameField.userFeedbackEnabled(enable)
    addressField.userFeedbackEnabled(enable)
    cityField.userFeedbackEnabled(enable)
    zipField.userFeedbackEnabled(enable)
    agreeButton.userFeedbackEnabled(enable)
    deleteButton.userFeedbackEnabled(enable)
    licenceCell.isUserInteractionEnabled = enable
    licenceCell.enable(enable)
  }
  
}

extension BankAccountController: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
   
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true;
    }
    
    func getStringAsCardNumberWithSartNumber(Number:Int,withString str:String) -> String{
        let arr = str.characters
        var CrediteCard : String = ""
        let len = str.characters.count-4
        if arr.count > (Number + len) {
            for (index, element ) in arr.enumerated(){
                if index >= Number && index < (Number + len) && element != "-" && element != " " {
                    CrediteCard = CrediteCard + String("X")
                }else{
                    CrediteCard = CrediteCard + String(element)
                }
            }
            return CrediteCard
        }else{
            print("\(Number) plus \(len) are grether than strings chatarter \(arr.count)")
        }
        print("\(CrediteCard)")
        return str
    }
    func customStringFormatting(of str: String) -> String {
        return str.characters.chunk(n: 4)
            .map{ String($0) }.joined(separator: " ")
    }
    
    
  func textFieldShouldClear(_ textField: UITextField) -> Bool {
    textField.text = nil
    updateButtons()
    return true
  }
  
}
extension Collection {
    public func chunk(n: IndexDistance) -> [SubSequence] {
        var res: [SubSequence] = []
        var i = startIndex
        var j: Index
        while i != endIndex {
            j = index(i, offsetBy: n, limitedBy: endIndex) ?? endIndex
            res.append(self[i..<j])
            i = j
        }
        return res
    }
}
