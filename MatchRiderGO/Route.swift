
import Foundation
import MapKit

struct Route {
  
  let id: Int
  let matchpoints: [Matchpoint]
  let polyline: MKPolyline
  let fromName: String
  let toName: String
  let fromShortName: String
  let toShortName: String
    
    let startMatchPointId: Int
    let destinationMatchPointId: Int
    
  
  init(json: JSON) {
    self.id = json["id"].int!
    self.fromName = json["startLocationCity"].string!
    self.fromShortName = json["startLocationCityShort"].string!
    self.toName = json["destinationLocationCity"].string!
    self.toShortName = json["destinationLocationCityShort"].string!
    self.matchpoints = json["matchPoints"].array!.map { json in
      return Matchpoint(json: json)
    }
    let rideRoute = json["rideRoute"].array!
    var coordinates = rideRoute.map { json -> CLLocationCoordinate2D in
      let latitude = json["latitude"].double!
      let longitude = json["longitude"].double!
      return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    polyline = MKPolyline(coordinates: &coordinates, count: coordinates.count)
    
    self.startMatchPointId = getIntValue(json: json["startMatchPointId"])
    self.destinationMatchPointId = getIntValue(json: json["destinationMatchPointId"])
  }
  
}
