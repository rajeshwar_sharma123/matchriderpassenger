
import UIKit

class OnboardingController: UIViewController {
  
  let viewControllers: [UIViewController] = [
    OnboardingPageController(nibName: "OnboardingViewFirst"),
    OnboardingPageController(nibName: "OnboardingView", imageName: "tutorial_2"),
    OnboardingPageController(nibName: "OnboardingView", imageName: "tutorial_3"),
    OnboardingPageController(nibName: "OnboardingView", imageName: "tutorial_4"),
    OnboardingPageController(nibName: "OnboardingView", imageName: "tutorial_5"),
    OnboardingPageController(nibName: "OnboardingViewLast")
  ]
  
  let pageViewController: UIPageViewController
  
  @IBOutlet var pageControl: UIPageControl!
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
    pageViewController = UIPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    pageViewController.setViewControllers([viewControllers.first!], direction: .forward, animated: false, completion: nil)
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    pageViewController.dataSource = self
    pageViewController.view.frame = view.bounds
    view.addSubview(pageViewController.view)
    addChildViewController(pageViewController)
  }
  
}

extension OnboardingController: UIPageViewControllerDataSource {
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    let index = viewControllers.index(of: viewController)!.advanced(by: -1)
    pageControl.currentPage = index + 1
    guard index >= viewControllers.startIndex else {
      //return viewControllers.last
        return nil
    }
    return viewControllers[index]
  }
  
  func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
    let index = viewControllers.index(of: viewController)!.advanced(by: 1)
    pageControl.currentPage = index - 1
    guard index < viewControllers.endIndex else {
      //return viewControllers.first
        return nil
    }
    return viewControllers[index]
  }
  
}
