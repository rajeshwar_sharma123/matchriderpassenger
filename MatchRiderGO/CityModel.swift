//
//  CityModel.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 23/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import Foundation
class CityModel{
    let cityId: Int
    let cityName: String
    let latitude: Float
    let longitude: Float
    let country: String
    
    init(json:JSON) {
        
        cityId = json["Id"].int!
        cityName = json["CityName"].string!
        latitude = json["Latitude"].float!
        longitude = json["Longitude"].float!
        country = json["Country"].string == nil ? "" : json["Country"].string!
    }
 }
