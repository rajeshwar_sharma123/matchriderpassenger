
import UIKit

class CreateUserController: UITableViewController {

  @IBOutlet var firstnameField: UITextField!
  @IBOutlet var lastnameField: UITextField!
  @IBOutlet var emailField: UITextField!
  @IBOutlet var passwordField: UITextField!
  @IBOutlet var repeatField: UITextField!
  @IBOutlet var licenceCell: UITableViewCell!
  @IBOutlet var signUpButton: UIButton!
  @IBOutlet var checkImage: UIImageView!
  var licenceChecked = false
  @IBAction func cancel() {
    self.dismiss(animated: true, completion: nil)
  }
  
  @IBAction func updateSignUpButton() {
    let nameSet = firstnameField.text != "" && lastnameField.text != ""
    let emailSet = emailField.text != ""
    let passwordSet = passwordField.text != ""
    let repeatSet = repeatField.text != ""
    //let passwordChecked = passwordField.text == repeatField.text
    signUpButton.userFeedbackEnabled(nameSet && emailSet && passwordSet && repeatSet && licenceChecked)
    checkImage.image = CheckedImage(licenceChecked)
  }
  
  @IBAction func signUp() {
    
    if NetworkReachability.isConnectedToNetwork(){
        signUpUser()
    }else{
        let controller = ActionControllerCommonAlert(NoNetworkError)
        self.present(controller, animated: true, completion: nil)
    }
    
  }
  
    func signUpUser(){
        if !isValidEmail(email: emailField.text!){
            
            let controller = ActionControllerRegister(false,ENTER_VALID_EMAIL_ID) {
            }
            self.present(controller, animated: true, completion: nil)
            return
        }
        if passwordField.text != repeatField.text{
            let controller = ActionControllerRegister(false,PASSWORD_MISMATCH) {
            }
            self.present(controller, animated: true, completion: nil)
            return
        }
        
        let email = emailField.text!
        let firstname = firstnameField.text!
        let lastname = lastnameField.text!
        let password = passwordField.text!
        enableAllButton(false)
        HttpRequest.sharedRequest.register(firstname, lastname: lastname, email: email, password: password) { success, error in
            
            let controller = ActionControllerRegister(success,error) {
                if success {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.enableAllButton(true)
                }
            }
            self.present(controller, animated: true, completion: nil)
        }
    }
    
  override func viewDidLoad() {
    super.viewDidLoad()
    updateSignUpButton()
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard tableView.cellForRow(at: indexPath) == licenceCell else {
      return
    }
    licenceChecked = !licenceChecked
    updateSignUpButton()
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let navi = segue.destination as! UINavigationController
    let target = navi.topViewController as! LicenceController
    target.legalPath = "MatchRiderLegal"
  }
  
  fileprivate func enableAllButton(_ enable: Bool) {
    firstnameField.userFeedbackEnabled(enable)
    lastnameField.userFeedbackEnabled(enable)
    emailField.userFeedbackEnabled(enable)
    passwordField.userFeedbackEnabled(enable)
    repeatField.userFeedbackEnabled(enable)
    signUpButton.userFeedbackEnabled(enable)
    licenceCell.isUserInteractionEnabled = enable
    navigationItem.leftBarButtonItem?.isEnabled = enable
  }
  
}

extension CreateUserController: UITextFieldDelegate {
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
}

extension UIControl {
  
  func userFeedbackEnabled(_ enabled: Bool) {
    if enabled {
      self.isEnabled = true
      self.alpha = 1
    } else {
      self.isEnabled = false
      self.alpha = 0.5
    }
  }
  
}
