
import UIKit
import MapKit

class RouteController: UIViewController {

  var ride: Ride!
  let locationManager =  CLLocationManager()
  @IBOutlet var mapView: MKMapView!
  @IBOutlet weak var startLabel: UITextField!
  @IBOutlet weak var destinationLabel: UITextField!
  var selectedMatchpoint: Matchpoint?
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.isToolbarHidden = false
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    mapView.delegate = self
    toolbarItems = toolbarItems! + [MKUserTrackingBarButtonItem(mapView: mapView)]
    title =  DateToString(ride.date!, dateStyle: .short)
    startLabel.text = ride.start?.title
    destinationLabel.text = ride.destination?.title
    locationManager.requestWhenInUseAuthorization()
    let matchpoints: [MKAnnotation] = [ride.start!, ride.destination!]
    self.mapView.addAnnotations(matchpoints)
    self.mapView.showAnnotations(matchpoints, animated: false)
    guard let token = Store.sharedStore.token else {
      return
    }
    HttpRequest.sharedRequest.driverLocation(token, rideId: ride.id) { location in
      guard let location = location else {
        return
      }
      let driverLocation = DriverLocation(lat: location.latitude, long: location.longitude)
      let annotations: [MKAnnotation] = [self.ride.start!, self.ride.destination!, driverLocation]
      self.mapView.addAnnotation(driverLocation)
      self.mapView.showAnnotations(annotations, animated: true)
    }
    requestRoute((ride.start?.coordinate)!, to: (ride.destination?.coordinate)!) { route in
      self.mapView.add(route.polyline)
    }
  }
  
  @IBAction func showSatellite(_ sender: UIBarButtonItem) {
    if mapView.mapType == .satellite {
      sender.tintColor = UIColor.lightGray
      mapView.mapType = .standard
      return
    }
    if mapView.mapType == .standard {
      sender.tintColor = MatchRiderColor
      mapView.mapType = .satellite
      return
    }
  }

}

extension RouteController: MKMapViewDelegate {
  
  func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
    if let annotation = view.annotation as? Matchpoint {
      selectedMatchpoint = annotation
      performSegue(withIdentifier: "showDetails", sender: nil)
    }
    mapView.deselectAnnotation(view.annotation, animated: false)
    selectedMatchpoint = nil
  }
  
  func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
    let polylineRenderer = MKPolylineRenderer(overlay: overlay)
    polylineRenderer.strokeColor = MatchRiderColor
    polylineRenderer.lineWidth = 4
    polylineRenderer.lineCap = .square
    return polylineRenderer
  }
  
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    guard !annotation.isKind(of: MKUserLocation.self) else {
      return nil
    }
    if let matchpoint = annotation as? Matchpoint {
      let view = MKAnnotationView(annotation: annotation, reuseIdentifier: "Matchpoint")
      view.centerOffset = CGPoint(x: 0, y: -18)
      switch matchpoint {
      case ride.start!:
        view.image = UIImage(named: "StartPoint")!
      case ride.destination!:
        view.image = UIImage(named: "EndPoint")!
      default:
        assertionFailure()
      }
      view.canShowCallout = false
      return view
    }
    if let driverLocation = annotation as? DriverLocation {
      let view = MKAnnotationView(annotation: driverLocation, reuseIdentifier: "DriverLocation")
       view.image = UIImage(named: "driver")!
      return view
    }
    return nil
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    let nav = segue.destination as! UINavigationController
    let detail = nav.topViewController as! MatchpointDetailController
    detail.matchpoint = selectedMatchpoint
    selectedMatchpoint = nil
  }
 }

private func requestRoute(_ from: CLLocationCoordinate2D, to: CLLocationCoordinate2D, callback: @escaping (_ route: MKRoute) -> Void) {
  let directionsRequest = MKDirectionsRequest()
  let startPlacemark = MKPlacemark(coordinate: from, addressDictionary: nil)
  let destinationPlacemark = MKPlacemark(coordinate: to, addressDictionary: nil)
  directionsRequest.source = MKMapItem(placemark: startPlacemark)
  directionsRequest.destination = MKMapItem(placemark: destinationPlacemark)
  directionsRequest.transportType = .automobile
  MKDirections(request: directionsRequest).calculate { response, error in
    guard let response = response else {
      return
    }
    guard let route = response.routes.first else {
      return
    }
    callback(route)
  }
}
