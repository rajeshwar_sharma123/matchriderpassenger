
import UIKit

class RideCell: UITableViewCell {
  
  static let identifier = "RideCell"
  
  @IBOutlet weak var driverName: UILabel!
  @IBOutlet weak var startLocation: UILabel!
  @IBOutlet weak var destinationLocation: UILabel!
  @IBOutlet weak var dateLabel: UILabel!
  @IBOutlet weak var driverView: IconView!
  
  func setStateWithDriverName(_ name: String, start: String, destination: String, date: Date, image: UIImage?) {
    driverName.text = name
    startLocation.text = start
    destinationLocation.text = destination
    dateLabel.text = getCompleteDateFromDate(date: date)
    
    if let image = image {
      driverView.image = image
    }
  }
  
}
