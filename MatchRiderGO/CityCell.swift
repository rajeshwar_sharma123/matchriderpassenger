//
//  CityCell.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 22/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet var cityNameLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func bindDataWithCell(city: CityModel){
        var selectedCity = -1
        if (userDefault.object(forKey: "CityId") != nil){
            selectedCity = userDefault.object(forKey: "CityId") as! Int
        }
        cityNameLabel.text = city.cityName
        if selectedCity == city.cityId {
           cityNameLabel.textColor = hexStringToUIColor(hex: "7DBE7F")
        }
        else {
            cityNameLabel.textColor = UIColor.black
        }
    }
    
}
