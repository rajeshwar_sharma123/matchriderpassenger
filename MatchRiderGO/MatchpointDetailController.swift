
import UIKit

class MatchpointDetailController: UITableViewController {

  var matchpoint: Matchpoint!
  
  @IBOutlet var nameLabel: UILabel!
  @IBOutlet var infoLabel: UILabel!
  @IBOutlet var streetLabel: UILabel!
  @IBOutlet var postalLabel: UILabel!
  @IBOutlet var photoView: UIImageView!
  
  override func viewWillAppear(_ animated: Bool) {
    nameLabel.text = matchpoint.info
    infoLabel.text = matchpoint.whereToStand
    streetLabel.text = matchpoint.street
    postalLabel.text = "\(matchpoint.postal) \(matchpoint.city)"
    if let image = HttpRequest.sharedRequest.imagesWithURL(matchpoint.photo) {
      photoView.image = image
    } else {
      HttpRequest.sharedRequest.image(matchpoint.photo) { image in
        self.photoView.image = image
      }
    }
  }
  
  @IBAction func cancel() {
    self.dismiss(animated: true, completion: nil)
  }
  
}
