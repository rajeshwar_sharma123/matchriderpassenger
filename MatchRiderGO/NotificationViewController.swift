//
//  NotificationViewController.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 06/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var segmentedControll: UISegmentedControl!
    @IBOutlet var tableView: UITableView!
    var threadsData : [ThreadsModel] = []
    let messageBadgeLbl = BadgeLabel()
    let ratingBadgeLbl = BadgeLabel()
    var rides = [Ride]()
    var networkViewObj: NetworkView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Nachrichten"
        setBadgeOnSegmentedControl()
        
    }
    override func viewWillAppear(_ animated: Bool) {
    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        checkNetworkAvailabilty()
        
    }
    
    func checkNetworkAvailabilty(){
         networkViewObj = self.networkView(view: contentView)
        if networkViewObj.isNetworkAvailable! {
            getRidesNotRated(Store.sharedStore.token!)
            getUnreadMessageNotificationCount(Store.sharedStore.token!)
            getNotRatedRideNotificationCount(Store.sharedStore.token!)
            getThreadsByPerson(Store.sharedStore.token!)
        }
         networkViewObj?.delegate = self
    }
    
    //Calling api to get Threads by Person
    fileprivate func getThreadsByPerson(_ token: Token) {
        self.view.showLoader()
        HttpRequest.sharedRequest.getThreadsByPerson(token) { threads in
            guard let threads = threads else {
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            self.threadsData = threads
            self.tableView.reloadData()
        }
    }
    fileprivate func getRidesNotRated(_ token: Token) {
        self.view.showLoader()
        HttpRequest.sharedRequest.getRidesNotRated(token) { rides in
            guard let rides = rides else {
                self.view.hideLoader()
                return
            }
            self.view.hideLoader()
            self.rides = rides
            self.tableView.reloadData()
        }
    }
    
    @IBAction func segmentedControllAction(_ sender: UISegmentedControl) {
        tableView.reloadData()
    }

    func setBadgeOnSegmentedControl()  {
       
        messageBadgeLbl.isHidden = true
        self.view.addSubview(messageBadgeLbl)
        self.view.addSubview(ratingBadgeLbl)
        ratingBadgeLbl.isHidden = true
    }
    func getUnreadMessageNotificationCount(_ token: Token){
        HttpRequest.sharedRequest.getUnreadThreadCount(token) { count in
            self.messageBadgeLbl.text = "\(count)"
            if count > 0{
            self.messageBadgeLbl.isHidden = false
            }
            else{
                self.messageBadgeLbl.isHidden = true
            }
        }
    }
    func getNotRatedRideNotificationCount(_ token: Token){
        HttpRequest.sharedRequest.getNotRatedRideCount(token) { count in
            self.ratingBadgeLbl.text = "\(count)"
            if count > 0{
            self.ratingBadgeLbl.isHidden = false
            }
            else{
               self.ratingBadgeLbl.isHidden = true
            }
       }
    }
    override func viewDidLayoutSubviews() {
        messageBadgeLbl.frame = CGRect(x: WIDTH/2-30, y: segmentedControll.frame.minY+4, width: 20, height: 20)
        ratingBadgeLbl.frame = CGRect(x: segmentedControll.frame.maxX-30, y: segmentedControll.frame.minY+4, width: 20, height: 20)
    }
}

extension NotificationViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if segmentedControll.selectedSegmentIndex == 0{
            if threadsData.count == 0{
                tableView.showNoNewsView()
                return threadsData.count
            }
         tableView.backgroundView = nil
        return threadsData.count
        }else{
            if rides.count == 0{
                tableView.showNoNotificationView()
                return rides.count
            }
            tableView.backgroundView = nil
            return rides.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        tableView.separatorStyle = .singleLine
        if segmentedControll.selectedSegmentIndex == 0{
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "NotificationCell") as! NotificationCell
        let thread = threadsData[indexPath.row]
        cell.bindDataWithCell(thread: thread)
        return cell
        }
        else{
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "RatingDetailCell") as! RatingDetailCell
            let ride = rides[indexPath.row]
            cell.bindDataWithCell(ride: ride)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if segmentedControll.selectedSegmentIndex == 0{
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        let thread = threadsData[indexPath.row]
        controller.messageThreadId = thread.messageThreadId
        controller.showDriverDetail = false
        self.navigationController?.pushViewController(controller, animated: true)
        }
        else{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "RatingViewController") as! RatingViewController
            controller.ride = rides[indexPath.row]
            self.navigationController?.pushViewController(controller, animated: true)
        }
    }
  }


extension NotificationViewController: NetworkViewDelegate{
    func refreshButtonClicked() {
        checkNetworkAvailabilty()
    }
}
