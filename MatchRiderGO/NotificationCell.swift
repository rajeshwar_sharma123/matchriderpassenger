//
//  NotificationCell.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 06/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {
    @IBOutlet var notificationLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var profilePic: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        accessoryType = .disclosureIndicator
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func bindDataWithCell(thread:ThreadsModel){
        nameLabel.text = thread.personName
        notificationLabel.text = thread.messagePosting
        if thread.isUnread{
            nameLabel.textColor = hexStringToUIColor(hex: "A19991")
            notificationLabel.textColor = hexStringToUIColor(hex: "A19991")
        }
        else{
            nameLabel.textColor = hexStringToUIColor(hex: MatchRiderThemeColor)
            notificationLabel.textColor = hexStringToUIColor(hex: MatchRiderThemeColor)
        }

        HttpRequest.sharedRequest.image(thread.personPhoto, useCache: true) { image in
            self.profilePic.image = image
            
        }

    }
    
}
