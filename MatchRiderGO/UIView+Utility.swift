//
//  UIView+Utility.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 24/03/17.
//  Copyright © 2017 LivelyCode. All rights reserved.
//

import Foundation
import RappleProgressHUD
extension UIView{
    func showLoader()
    {
        RappleActivityIndicatorView.startAnimatingWithLabel("Loading...", attributes: RappleAppleAttributes)
       
    }
    func showLoaderWithMessage(message:String)
    {
        RappleActivityIndicatorView.startAnimatingWithLabel(message, attributes: RappleAppleAttributes)
    }
    func hideLoader()
    {
        RappleActivityIndicatorView.stopAnimating()
    }

}
