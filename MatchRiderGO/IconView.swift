
import UIKit

class IconView: UIImageView {
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    contentMode = .scaleAspectFill
    image = UIImage(named: "Person")?.withRenderingMode(.alwaysTemplate)
    tintColor = MatchRiderColor
  }
    
    override func layoutSubviews() {
        layer.cornerRadius = frame.width / 2;
        layer.masksToBounds = true
    }
  
}
