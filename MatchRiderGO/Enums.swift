//
//  Enums.swift
//  ChatDemo
//
//  Created by daffolapmac on 21/12/16.
//  Copyright © 2016 daffolapmac. All rights reserved.
//

import Foundation
enum TriangleMode: Int {
    case Left = 0, Up = 1, Right = 2, Down = 3
}
