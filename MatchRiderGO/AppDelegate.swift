
import UIKit
import Stripe
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
import IQKeyboardManagerSwift
import Fabric
import Crashlytics
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    /// The callback to handle data message received via FCM for devices running iOS 10 or above.
    
    
    var window: UIWindow?
    let mainNavigation: UINavigationController = {
        let controller = UINavigationController()
        controller.isNavigationBarHidden = true
        controller.navigationBar.titleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.white
        ]
        controller.navigationBar.barTintColor = MatchRiderColor
        controller.navigationBar.isTranslucent = false
        return controller
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        //Thread.sleep(forTimeInterval: 3)
        // test stripe key sk_test_gBIBJfJUK9r5FnQQrvgprgD6
        // live stripe pk_live_4Mmz0Ju6eo7gtLOHNPBlUzGX
        
        
        Fabric.with([Crashlytics.self])
        Stripe.setDefaultPublishableKey("pk_live_4Mmz0Ju6eo7gtLOHNPBlUzGX")
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        Mixpanel.sharedInstance(withToken: "d9bddb49dda7f182c7a7e358f792d9d2")
        let windowFrame = UIScreen.main.bounds
        window = UIWindow(frame: windowFrame)
        window?.tintColor = MatchRiderColor
        window?.rootViewController = mainNavigation
        window?.makeKeyAndVisible()
        UINavigationBar.appearance().tintColor = UIColor.white
        IQKeyboardManager.sharedManager().enable = true
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
        // [END register_for_notifications]
        
        FIRApp.configure()
        
        // Add observer for InstanceID token refresh callback.
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.tokenRefreshNotification),
                                               name: .firInstanceIDTokenRefresh,
                                               object: nil)
        
   
        
        let store = Store.sharedStore
        if store.onboardingComplete && store.token != nil {
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bookedRides")
            mainNavigation.setViewControllers([controller], animated: false)
        }
        if store.onboardingComplete && store.token == nil {
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginController")
            mainNavigation.setViewControllers([controller], animated: false)
        }
        if !store.onboardingComplete {
            let controller = OnboardingController(nibName: "OnboardingMain", bundle: nil)
            mainNavigation.setViewControllers([controller], animated: false)
        }
        return true
           
    }
    
    func setUpdateView(){
        let updateView = UIView()
        updateView.frame = self.window!.bounds
        updateView.backgroundColor = UIColor.white
        self.window?.addSubview(updateView)
        
     }
    
    
    
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
//    func isUpdateAvailable() -> Bool {
//        var upgradeAvailable = false
//        let bundle = Bundle.main
//        if let infoDictionary = bundle.infoDictionary {
//
//            let identifier = infoDictionary["CFBundleIdentifier"] as? String
//            let storeInfoURL: String = "http://itunes.apple.com/lookup?bundleId=\(identifier!)"
//            let urlOnAppStore = NSURL(string: storeInfoURL)
//            if let dataInJSON = NSData(contentsOf: urlOnAppStore! as URL) {
//             if let dict: NSDictionary = try! JSONSerialization.jsonObject(with: dataInJSON as Data, options: JSONSerialization.ReadingOptions.allowFragments) as! [String: AnyObject] as NSDictionary? {
//                    if let results:NSArray = dict["results"] as? NSArray {
//                        if let version = ((results[0] as! NSDictionary).value(forKey: "version")!) as? String {
//              
//                            if let currentVersion = infoDictionary["CFBundleShortVersionString"] as? String {
//                    
//                                print("\(version)")
//                                if version != currentVersion {
//                                    upgradeAvailable = true
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        return upgradeAvailable
//}
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        connectToFcm()
        if (userDefault.object(forKey: "InActiveTime") != nil){
            let time = userDefault.object(forKey: "InActiveTime") as! Date
            let difference = Calendar.current.dateComponents([.minute], from: time, to: Date())
            if difference.minute! > 10{
                userDefault.removeObject(forKey: "InActiveTime")
                let store = Store.sharedStore
                if store.onboardingComplete && store.token != nil {
                    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "bookedRides")
                    mainNavigation.setViewControllers([controller], animated: false)
                    mainNavigation.dismiss(animated: false, completion: nil)
                }
            }
            
        }
        
        if updateAppCheck {
            showAlertForNewVersionUpdate()
        }
    }
    func applicationWillResignActive(_ application: UIApplication) {
        userDefault.set(Date(), forKey: "InActiveTime")
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        FIRMessaging.messaging().disconnect()
        
    }
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        print("Message ID: \(userInfo["gcm.message_id"]!)")
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        let contoller =  Utility.topViewController()
        let chatDetail = getChatDetailData(userInfo: userInfo)
        if (application.applicationState == .active ) {
            
            let rideSpecificFlag = getRideSpecificDetail(userInfo: userInfo)
            if rideSpecificFlag{
                
                let contoller =  Utility.topViewController()
                if contoller is BookedRidesController || contoller is RideDetailsController{
                    
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
                }
                showNotificationWhenAppIsActive(userInfo: userInfo)
                
            }else{
                
                if contoller is MessageViewController{
                    
                    if chatDetail.messageThreadId == Store.sharedStore.currentMessageThreadId || chatDetail.messageThreadId == "Default"{
                        postNotification(chatDetail: chatDetail, check: true)
                    }else{
                        showNotificationWhenAppIsActive(userInfo: userInfo)
                    }
                }else{
                    showNotificationWhenAppIsActive(userInfo: userInfo)
                }
            }
            return
        }
        
        let rideSpecificFlag = getRideSpecificDetail(userInfo: userInfo)
        if rideSpecificFlag{
            
            let contoller =  Utility.topViewController()
            if contoller is BookedRidesController || contoller is RideDetailsController{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
            }else{
                redirectToBookedRidesController()
            }
            
        }else{
            
            if contoller is MessageViewController{
                if chatDetail.messageThreadId != Store.sharedStore.currentMessageThreadId{
                    postNotification(chatDetail: chatDetail, check: true)
                }
            }else{
                redirectToMessageViewController(chatDetail: chatDetail)
            }
        }
        
    }
    func showNotificationWhenAppIsActive(userInfo: [AnyHashable: Any]){
        let tmpDic = userInfo["aps"] as! NSDictionary
        let alertDic = tmpDic["alert"] as! NSDictionary
        let localNotification = UILocalNotification()
        localNotification.soundName = UILocalNotificationDefaultSoundName
        localNotification.alertTitle = alertDic["title"] as? String
        localNotification.alertBody = alertDic["body"] as? String
        localNotification.fireDate = Date()
        UIApplication.shared.scheduleLocalNotification(localNotification)
    }
    // [START refresh_token]
    func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = FIRInstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
        }
        
        // Connect to FCM since connection may have failed when attempted before having a token.
        connectToFcm()
    }
    // [START connect_to_fcm]
    func connectToFcm() {
        FIRMessaging.messaging().connect { (error) in
            if error != nil {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        
        // With swizzling disabled you must set the APNs token here.
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.sandbox)
    }
}
// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let rideSpecificFlag = getRideSpecificDetail(userInfo: notification.request.content.userInfo)
        if rideSpecificFlag{
            
            let contoller =  Utility.topViewController()
            if contoller is BookedRidesController || contoller is RideDetailsController{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
            }
            completionHandler(UNNotificationPresentationOptions.alert)
            
        }else{
            
            let contoller =  Utility.topViewController()
            if contoller is MessageViewController{
                let chatDetail = getChatDetailData(userInfo:notification.request.content.userInfo)
                if chatDetail.messageThreadId == Store.sharedStore.currentMessageThreadId || chatDetail.messageThreadId == "Default"{
                    postNotification(chatDetail: chatDetail, check: true)
                }
                else{
                    completionHandler(UNNotificationPresentationOptions.alert)
                }
            }else{
                completionHandler(UNNotificationPresentationOptions.alert)
            }
        }
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let rideSpecificFlag = getRideSpecificDetail(userInfo: response.notification.request.content.userInfo)
        if rideSpecificFlag{
            
            let contoller =  Utility.topViewController()
            if contoller is BookedRidesController || contoller is RideDetailsController{
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
            }else{
                redirectToBookedRidesController()
            }
            
        }else{
            let chatDetail = getChatDetailData(userInfo: response.notification.request.content.userInfo)
            let contoller =  Utility.topViewController()
            if contoller is MessageViewController{
                
                if chatDetail.messageThreadId == Store.sharedStore.currentMessageThreadId{
                    postNotification(chatDetail: chatDetail, check: false)
                }else{
                    //Reload chat
                    postNotification(chatDetail: chatDetail, check: false)
                }
                
            }else{
                redirectToMessageViewController(chatDetail: chatDetail)
            }
        }
    }
}

func getChatDetailData(userInfo: [AnyHashable: Any]) -> ChatModel {
    let message = userInfo["message"] as! String
    let data = message.data(using: .utf8)
    let json = JSON(data: data!)
    return ChatModel(json: json)
}

func postNotification(chatDetail: ChatModel, check: Bool){
    let dict:[String: Any] = ["chatDetail": chatDetail,"check":check]
    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "FCMNotification"), object: nil, userInfo: dict)
}
func getRideSpecificDetail(userInfo: [AnyHashable: Any])->Bool{
    let message = userInfo["rideSpecific"] as! String
    let data = message.data(using: .utf8)
    let json = JSON(data: data!)
    print("rideSpecific...\(json.bool)")
    return json.bool!
}
func getRideId(userInfo: [AnyHashable: Any])->Int{
    let message = userInfo["rideId"] as! String
    let data = message.data(using: .utf8)
    let json = JSON(data: data!)
    print("rideSpecific...\(json.int)")
    return json.int!
}


func redirectToMessageViewController(chatDetail: ChatModel){
    let navController = Utility.getCurrentNavigationController()
    let messageViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
    messageViewController.messageThreadId = chatDetail.messageThreadId
    navController.pushViewController(messageViewController, animated: true)
}

func redirectToBookedRidesController(){
    let navController = Utility.getCurrentNavigationController()
    navController.dismiss(animated: true, completion: nil)
    
}

// [END ios_10_message_handling]

extension AppDelegate : FIRMessagingDelegate {
    // Receive data message on iOS 10 devices while app is in the foreground.
    func applicationReceivedRemoteMessage(_ remoteMessage: FIRMessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
}

