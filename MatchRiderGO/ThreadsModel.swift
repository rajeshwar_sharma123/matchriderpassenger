//
//  ThreadsModel.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 07/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import Foundation

struct ThreadsModel {
    
    let messagePostId: Int
    let messageThreadId: String
    let personUserId: String
    let messageThreadParticipantId: String
    let driverRequestId: Int
    let messagePosting : String
    let dateCreated: String
    let personId: Int
    let personName: String
    let personPhoto: String
    let isUnread : Bool
    
    init(json: JSON) {
        
        personName = json["PersonName"].string != nil ? json["PersonName"].string!: ""
        messagePostId = json["MessagePostId"].int!
        messageThreadId = json["MessageThreadId"].string!
        messageThreadParticipantId = getStringValue(json: json["MessageThreadParticipantId"])
        driverRequestId = json["DriverRequestId"].int!
        messagePosting = json["MessagePosting"].string!
        dateCreated = json["DateCreated"].string!
        personId = json["PersonId"].int!
        
        personPhoto = json["PersonPhoto"].string != nil ? json["PersonPhoto"].string!: ""
        isUnread = json["IsUnread"].bool!
        personUserId = json["PersonUserId"].string!
    }
    
}
