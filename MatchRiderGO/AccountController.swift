
import UIKit
import Crashlytics

class AccountController: UITableViewController {
    
    @IBOutlet var redeemButton: UIButton!
   
    //@IBOutlet var notificationLabel: UILabel!
    @IBOutlet var menuTableView: UITableView!
    var icon: UIImage?
    var doneButton: UIBarButtonItem!
    var saveButton: UIBarButtonItem!
    var redeemField: UITextField!
    var impersonateField: UITextField!
    let totalBadgeLbl = BadgeLabel()
    
    var textView: TextView!
    var iconView: IconView!
    var userDescription = ""
    var creditAmmount = ""
    let rightButton = UIButton(type: .custom)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Konto"
        rightButton.tag = 1

        rightButton.setImage(UIImage(named: "ic_edit"), for: .normal)
        rightButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        rightButton.addTarget(self, action: #selector(self.rightButtonAction), for: .touchUpInside)
        let rightBtnItem = UIBarButtonItem(customView: rightButton)
        navigationItem.setRightBarButton(rightBtnItem, animated: true)
        getUserAccountDetail()
        updateCredit()
    }
   //raj
    func getUserAccountDetail(){
        guard let token = Store.sharedStore.token else {
            return
        }
        HttpRequest.sharedRequest.accountDetails(token) { user in
            guard let user = user else{
                return
            }
            self.userDescription = user.description!
            Store.sharedStore.personUserId = user.personUserId
            Store.sharedStore.personUserPhoto = user.photo
            Store.sharedStore.personName = user.firstName
            Store.sharedStore.isAdmin = user.isAdmin
            
            self.menuTableView.reloadData()

        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setBadgeOnNotificationLabel()
    }
    //raj
    func setBadgeOnNotificationLabel(){
        guard let token = Store.sharedStore.token else {
            return
        }
        getTotalNotificationCount(token)

    }
    //raj
    override func viewDidLayoutSubviews() {
     //   notificationLabel.badgeView.position = .centerRight
    }
    //raj
    func getTotalNotificationCount(_ token: Token){
        
        HttpRequest.sharedRequest.getTotalNotificationsCount(token) { count in
            Store.sharedStore.totalNotificationCount = count
            if count == 0{
                self.totalBadgeLbl.isHidden = true
            }
            self.totalBadgeLbl.text = "\(count)"
            self.menuTableView.reloadData()
        }
    }
    
     func logout() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        DispatchQueue.main.async {
            Store.sharedStore.reset()
            FBSDKLoginManager().logOut()
            let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginController")
            delegate.mainNavigation.setViewControllers([controller], animated: false)
            delegate.mainNavigation.dismiss(animated: true, completion: nil)
        }
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        if Store.sharedStore.isAdmin{
           return 7
        }
        return 6
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return  1
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // rightButton.tag
        switch indexPath.section {
        case 0:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "UserDetailCell") as! UserDetailCell
            cell.textView.placeholder = "Wer  bist du?"
            let iconViewTap = UITapGestureRecognizer(target: self, action:#selector(takePicture))
            cell.iconView.addGestureRecognizer(iconViewTap)
            
            if rightButton.tag != 2 {
            
            cell.iconView.tag = 1
            cell.textView.isEditable = false
            cell.iconView.isUserInteractionEnabled = false
            }
            cell.textView.delegate = self
            textView = cell.textView
            iconView = cell.iconView
            cell.textView.text = userDescription
            if Store.sharedStore.personUserPhoto != "" {
                let path = Store.sharedStore.personUserPhoto
                let truncated = path!.substring(to: path!.index(before: path!.endIndex))
                let exactPath = "\(truncated)l"
                HttpRequest.sharedRequest.image(exactPath, useCache: false) { image in
                    if self.rightButton.tag != 2 || cell.iconView.tag == 1{
                        cell.iconView.image = image
                    }
                }
            }

            return cell
        case 1:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "AccountMessageCell") as! AccountMessageCell
            
            self.totalBadgeLbl.text = "\(Store.sharedStore.totalNotificationCount!)"
            totalBadgeLbl.isHidden = true
            totalBadgeLbl.frame = CGRect(x: cell.messageLabel.frame.maxX-45, y: 2, width: 25, height: 25)
            cell.messageLabel.addSubview(totalBadgeLbl)
            if Store.sharedStore.totalNotificationCount != 0{
                totalBadgeLbl.isHidden = false
            }
            return cell
        case 2:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "CreditCell") as! CreditCell
            cell.creditLabel.text = creditAmmount
            cell.redeemButton.addTarget(self, action: #selector(self.redeem), for: .touchUpInside)
            return cell
        case 3,4,5:
            
                let cell = self.tableView.dequeueReusableCell(withIdentifier: "CommonCell") as! CommonCell
                if indexPath.section == 3{
                    cell.nameLabel.text = "Bezahlmethode"
                } else if indexPath.section == 4{
                    cell.nameLabel.text = "Bevorzugte Stadt ändern"
                }else if indexPath.section == 5{
                    cell.nameLabel.text = "Imitieren"
                }
                if indexPath.section == 5{
                    if !Store.sharedStore.isAdmin{
                        let cell = self.tableView.dequeueReusableCell(withIdentifier: "LogoutCell") as! LogoutCell
                        return cell
                    }
                }
                
            return cell
        case 6:
            let cell = self.tableView.dequeueReusableCell(withIdentifier: "LogoutCell") as! LogoutCell
            return cell


        default:
            break
        }
        
       return UITableViewCell()
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
        return 120
        }
        return 64
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "NotificationViewController") as! NotificationViewController
            self.navigationController?.pushViewController(controller, animated: true)
        }
        else if indexPath.section == 3{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "BankAccountController") as! BankAccountController
             self.navigationController?.pushViewController(controller, animated: true)
        }
        else if indexPath.section == 4{
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "CityViewController") as! CityViewController
            controller.checkFirstTime = false
            self.navigationController?.pushViewController(controller, animated: true)
        }
       else if indexPath.section == 5{
            if !Store.sharedStore.isAdmin{
                logout()
                return
            }
            setImpersonate()
        }
            
        else if indexPath.section == 6{
            logout()
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        userDescription = textView.text
    }
    
    @IBAction func cancel() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func redeem() {
        let controller = ActionControllerRedeem {
            guard let token = Store.sharedStore.token else {
                return
            }
            guard let code = self.redeemField.text else {
                return
            }
            HttpRequest.sharedRequest.redeem(token, code: code) { success in
                if !success {
                    let controller = ActionControllerRedeemFailed()
                    self.present(controller, animated: true, completion: nil)
                }
                self.updateCredit()
            }
        }
        controller.addTextField { textField in
            self.redeemField = textField
            self.redeemField.delegate = self
            self.redeemField.placeholder = "Gutscheincode"
        }
        present(controller, animated: true, completion: nil)
    }
    
    func setImpersonate(){
        
        let controller = ActionControllerImpersonate {
            guard let token = Store.sharedStore.token else {
                return
            }
            if self.impersonateField.text!.isEmpty{
                return
            }
            self.view.showLoader()
            var userId: String? = nil
            var email: String? = nil
            if self.impersonateField.text!.isNumber{
                userId = self.impersonateField.text!
            }else{
                email = self.impersonateField.text!
            }
            HttpRequest.sharedRequest.getUserTokenForAdmin(token, userId: userId, email: email, callback: { (token, error) in
                
                if error != nil{
                    self.view.hideLoader()
                    let controller = ActionControllerCommonAlert(error)
                    self.present(controller, animated: true, completion: nil)
                    return
                }
                
                guard let token = token else{
                    self.view.hideLoader()
                    return
                }
                self.view.hideLoader()
                Store.sharedStore.token = token
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateProfilePick"), object: nil)
                self.dismiss(animated: true, completion: nil)
                
            })
        }
        controller.addTextField { textField in
            self.impersonateField = textField
            self.impersonateField.delegate = self
            self.impersonateField.placeholder = "Enter person Id or email"
        }
        present(controller, animated: true, completion: nil)

    }
    
     func takePicture() {
        let controller = UIAlertController(title: "Foto", message: nil, preferredStyle: .actionSheet)
        let cancel = UIAlertAction(title: "Abbrechen", style: .cancel) { action in
            controller.dismiss(animated: true, completion: nil)
        }
        let camera = UIAlertAction(title: "Kamera", style: .default) { action in
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.cameraDevice = .front
            picker.showsCameraControls = true
            picker.allowsEditing = false
            picker.delegate = self
            self.present(picker, animated: false, completion: nil)
        }
        let library = UIAlertAction(title: "Fotosammlung", style: .default) { action in
            let picker = UIImagePickerController()
            picker.navigationBar.isTranslucent = false
            picker.navigationBar.titleTextAttributes = [
                NSForegroundColorAttributeName: UIColor.white
            ]
            picker.navigationBar.barTintColor = MatchRiderColor
            picker.sourceType = .photoLibrary
            picker.allowsEditing = false
            picker.delegate = self
            self.present(picker, animated: true, completion: nil)
        }
        controller.addAction(cancel)
        controller.addAction(camera)
        controller.addAction(library)
        present(controller, animated: true, completion: nil)
        
        
    }
    //raj
    func rightButtonAction(_ sender: UIButton){
   
        if rightButton.tag == 1{
            textView.isEditable = true
            iconView.isUserInteractionEnabled = true
            rightButton.setImage(UIImage(named: "ic_done_white"), for: .normal)
            rightButton.tag = 2
        }else{
            textView.isEditable = false
            iconView.isUserInteractionEnabled = false
            rightButton.setImage(UIImage(named: "ic_edit"), for: .normal)
            rightButton.tag = 1
            save()
        }
    }
       //raj
    func save() {
        defer {
            
            self.dismiss(animated: true, completion: nil)
        }
        guard let token = Store.sharedStore.token else {
            return
        }
        let description = textView.text
        HttpRequest.sharedRequest.updateAccountDetails(token, description: description, photo: icon)
    }
    //raj
    fileprivate func updateCredit() {
        guard let token = Store.sharedStore.token else {
            return
        }
        HttpRequest.sharedRequest.credit(token) { credit in
            guard let credit = credit else {
               
                self.creditAmmount = "Guthaben: ?"
                return
            }
          
            self.creditAmmount = "Guthaben: \(CentsToString(credit))"
            self.menuTableView.reloadData()
        }
    }
    
}

extension AccountController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
    }
    
}

extension AccountController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //raj
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        defer {
            self.dismiss(animated: true, completion: nil)
        }
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
            return
        }
        
        let size = CGSize(width: 480, height: 640)
        let imgTmp = image.resizeImage(targetSize: size)
        icon = imgTmp
        iconView.tag = 2
        iconView.image = imgTmp
    }
    
}

extension AccountController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
