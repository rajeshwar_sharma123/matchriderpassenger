
import UIKit

class OnboardingPageController: UIViewController {
  
  @IBOutlet var imageView: UIImageView!
  let imageName: String?
  
  init(nibName: String) {
    imageName = nil
    super.init(nibName: nibName, bundle: nil)
  }
  
  init(nibName: String, imageName: String) {
    self.imageName = imageName
    super.init(nibName: nibName, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    if let imageName = imageName {
      imageView.image = UIImage(named: imageName)
    }
   }
  
  @IBAction func done() {
    Store.sharedStore.onboardingComplete = true
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "loginController")
    delegate.mainNavigation.setViewControllers([controller], animated: true)
  }
  
}
