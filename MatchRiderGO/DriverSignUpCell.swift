//
//  DriverSignUpCell.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 20/03/17.
//  Copyright © 2017 LivelyCode. All rights reserved.
//

import UIKit

class DriverSignUpCell: UITableViewCell {
    
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var ratingView: Rating!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var driverView: IconView!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func bindDataWithCell(driver: DriverSignUpModel){
        let price = convertAmountToString(driver.approximateRegularRouteMonthlyPayment)
        messageLabel.text = "Verdiene monatlich \(price) mit deinen Pendelfahrten!"
        driverName.text = Store.sharedStore.personName
        ratingView.setCount(5)
        if let path = Store.sharedStore.personUserPhoto {
            HttpRequest.sharedRequest.image(path, useCache: true) { image in
                self.driverView.image = image
            }
        }
    }
    
}
