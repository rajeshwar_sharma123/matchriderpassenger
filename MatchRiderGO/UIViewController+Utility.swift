//
//  UIViewController+Utility.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 28/12/16.
//  Copyright © 2016 LivelyCode. All rights reserved.
//

import Foundation
extension UIViewController{
   
    func networkView(view:UIView)-> NetworkView!{
        let networkView: NetworkView!
        networkView = NetworkView.initNetworkView()
        networkView?.frame = view.bounds
        if !NetworkReachability.isConnectedToNetwork(){
            view.addSubview(networkView)
            networkView.isNetworkAvailable = false
        }
        else{
          networkView.isNetworkAvailable = true
        }
       return networkView
    }
}
