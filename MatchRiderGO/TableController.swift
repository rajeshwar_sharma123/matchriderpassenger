
class TableController: UITableViewController {
  
  @IBOutlet weak var messageLabel: UILabel!
    
  func showTableView() {
    self.tableView.backgroundView = nil
    self.tableView.separatorStyle = .singleLine
    self.tableView.reloadData()
  }
  
  func showMessage(_ string: String) {
    tableView.backgroundView = Bundle.main.loadNibNamed("NoRides", owner: self, options: nil)?.first as? UIView
    tableView.separatorStyle = .none
    messageLabel.text = string
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.isToolbarHidden = true
  }
  
}
