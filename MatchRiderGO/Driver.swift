
struct Driver {
  
  let id: Int
  let firstName: String
  let lastName: String
  let email: String
  let photo: String
  let score: Int
  let phone: String
  let description: String
  let gender: Gender
  
  init(json: JSON) {
    id = getIntValue(json: json["id"])
    firstName = getStringValue(json: json["firstName"])
    lastName = getStringValue(json: json["lastName"])
    email = getStringValue(json: json["email"])
    photo = getStringValue(json: json["photo"])
    score = getIntValue(json: json["score"])
    phone = getStringValue(json: json["phone"])
    description = getStringValue(json: json["description"])
    gender = Gender(json: json)
    
    //getStringValue(json: json["userId"])
  }
  
}
