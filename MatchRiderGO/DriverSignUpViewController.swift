//
//  DriverSignUpViewController.swift
//  MatchRiderGO
//
//  Created by daffolapmac on 20/03/17.
//  Copyright © 2017 LivelyCode. All rights reserved.
//

import UIKit

class DriverSignUpViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet var webView: UIWebView!
    var driverSignUp: DriverSignUpModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Fahrer Sign-Up"
        webView.delegate = self
        self.view.showLoader()
        webView.loadRequest(NSURLRequest(url: NSURL(string: driverSignUp.driverSignupURL)! as URL) as URLRequest)
     
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        self.view.hideLoader()
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.view.hideLoader()
    }
   
}
