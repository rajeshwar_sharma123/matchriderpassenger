
import UIKit
import MapKit
import Foundation


//
//  UIColorExtension.swift
//  HEXColor
//
//  Created by R0CKSTAR on 6/13/14.
//  Copyright (c) 2014 P.D.Q. All rights reserved.
//

import UIKit

/**
 MissingHashMarkAsPrefix:   "Invalid RGB string, missing '#' as prefix"
 UnableToScanHexValue:      "Scan hex error"
 MismatchedHexStringLength: "Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8"
 */
public enum UIColorInputError : Error {
  case missingHashMarkAsPrefix,
  unableToScanHexValue,
  mismatchedHexStringLength
}

extension UIColor {
  /**
   The shorthand three-digit hexadecimal representation of color.
   #RGB defines to the color #RRGGBB.
   
   - parameter hex3: Three-digit hexadecimal value.
   - parameter alpha: 0.0 - 1.0. The default is 1.0.
   */
  public convenience init(hex3: UInt16, alpha: CGFloat = 1) {
    let divisor = CGFloat(15)
    let red     = CGFloat((hex3 & 0xF00) >> 8) / divisor
    let green   = CGFloat((hex3 & 0x0F0) >> 4) / divisor
    let blue    = CGFloat( hex3 & 0x00F      ) / divisor
    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
  
  /**
   The shorthand four-digit hexadecimal representation of color with alpha.
   #RGBA defines to the color #RRGGBBAA.
   
   - parameter hex4: Four-digit hexadecimal value.
   */
  public convenience init(hex4: UInt16) {
    let divisor = CGFloat(15)
    let red     = CGFloat((hex4 & 0xF000) >> 12) / divisor
    let green   = CGFloat((hex4 & 0x0F00) >>  8) / divisor
    let blue    = CGFloat((hex4 & 0x00F0) >>  4) / divisor
    let alpha   = CGFloat( hex4 & 0x000F       ) / divisor
    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
  
  /**
   The six-digit hexadecimal representation of color of the form #RRGGBB.
   
   - parameter hex6: Six-digit hexadecimal value.
   */
  public convenience init(hex6: UInt32, alpha: CGFloat = 1) {
    let divisor = CGFloat(255)
    let red     = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
    let green   = CGFloat((hex6 & 0x00FF00) >>  8) / divisor
    let blue    = CGFloat( hex6 & 0x0000FF       ) / divisor
    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
  
  /**
   The six-digit hexadecimal representation of color with alpha of the form #RRGGBBAA.
   
   - parameter hex8: Eight-digit hexadecimal value.
   */
  public convenience init(hex8: UInt32) {
    let divisor = CGFloat(255)
    let red     = CGFloat((hex8 & 0xFF000000) >> 24) / divisor
    let green   = CGFloat((hex8 & 0x00FF0000) >> 16) / divisor
    let blue    = CGFloat((hex8 & 0x0000FF00) >>  8) / divisor
    let alpha   = CGFloat( hex8 & 0x000000FF       ) / divisor
    self.init(red: red, green: green, blue: blue, alpha: alpha)
  }
  
  /**
   The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, throws error.
   
   - parameter rgba: String value.
   */
  public convenience init(rgba_throws rgba: String) throws {
    guard rgba.hasPrefix("#") else {
      throw UIColorInputError.missingHashMarkAsPrefix
    }
    
    guard let hexString: String = rgba.substring(from: rgba.characters.index(rgba.startIndex, offsetBy: 1)),
      var   hexValue:  UInt32 = 0
      , Scanner(string: hexString).scanHexInt32(&hexValue) else {
        throw UIColorInputError.unableToScanHexValue
    }
    
    switch (hexString.characters.count) {
    case 3:
      self.init(hex3: UInt16(hexValue))
    case 4:
      self.init(hex4: UInt16(hexValue))
    case 6:
      self.init(hex6: hexValue)
    case 8:
      self.init(hex8: hexValue)
    default:
      throw UIColorInputError.mismatchedHexStringLength
    }
  }
  
  /**
   The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, fails to default color.
   
   - parameter rgba: String value.
   */
  public convenience init(rgba: String, defaultColor: UIColor = UIColor.clear) {
    guard let color = try? UIColor(rgba_throws: rgba) else {
      self.init(cgColor: defaultColor.cgColor)
      return
    }
    self.init(cgColor: color.cgColor)
  }
  
  /**
   Hex string of a UIColor instance.
   
   - parameter rgba: Whether the alpha should be included.
   */
  public func hexString(_ includeAlpha: Bool) -> String {
    var r: CGFloat = 0
    var g: CGFloat = 0
    var b: CGFloat = 0
    var a: CGFloat = 0
    self.getRed(&r, green: &g, blue: &b, alpha: &a)
    
    if (includeAlpha) {
      return String(format: "#%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
    } else {
      return String(format: "#%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
    }
  }
  
  open override var description: String {
    return self.hexString(true)
  }
  
  open override var debugDescription: String {
    return self.hexString(true)
  }
}

func CheckedImage(_ checked: Bool) -> UIImage {
  if checked {
    return UIImage(named: "checked")!
  } else {
    return UIImage(named: "unchecked")!
  }
}

func DateToString(_ date: Date, relative: Bool = true, dateStyle: DateFormatter.Style = .none, timeStyle: DateFormatter.Style = .none) -> String {
  let formatter = DateFormatter()
  formatter.locale = Locale(identifier: "de_DE")
  formatter.dateStyle = dateStyle
  formatter.timeStyle = timeStyle
  formatter.doesRelativeDateFormatting = relative
  return formatter.string(from: date)
}
func getCompleteDateFromDate(date: Date)-> String{
    let dateFormatter = DateFormatter()
    dateFormatter.locale = Locale(identifier: "de_DE")
    dateFormatter.dateFormat = "EEEE, d. MMM HH:mm"
    return dateFormatter.string(from: date)
}

func getDateWithDay(_ date: Date,timeFlag:Bool)-> String{
//    formatter.dateFormat =  "EEEE',' MMMM dd',' yyyy"
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "de_DE")
    let difference = Calendar.current.dateComponents([.day,.hour,.minute], from: Date(), to: date)
    if difference.day! == 0 || difference.day! == 1{
        formatter.dateStyle = .short
        timeFlag ?(formatter.timeStyle = .short):(formatter.timeStyle = .none)
        formatter.doesRelativeDateFormatting = true
        return formatter.string(from: date)
    }
    else if difference.day! > 1 && difference.day! < 6{
        timeFlag ?(formatter.dateFormat = "EEEE, HH:mm"):(formatter.dateFormat = "EEEE")
        return  formatter.string(from: date).capitalized
        
    }else{
        timeFlag ?(formatter.dateFormat = "EEEE, MMM dd, HH:mm"):(formatter.dateFormat = "EEEE, MMM dd")
        return  formatter.string(from: date).capitalized
    }

}

func StringToDate(_ string: String) -> Date {
  let dateFormatter = DateFormatter()
  dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
  return dateFormatter.date(from: string)!
}

func DateWithOffset(_ date: Date, minutes: Int = 0, days: Int = 0) -> Date {
  let calendar = Calendar.autoupdatingCurrent
  let minutesOffset = (calendar as NSCalendar).date(byAdding: .minute, value: minutes, to: date, options: [])!
  return (calendar as NSCalendar).date(byAdding: .day, value: days, to: minutesOffset, options: [])!
}

func RouteDirectionString(_ from: String, to: String) -> String {
  return "\(from) ➡︎ \(to)"
}

func CentsToString(_ cents: Int) -> String {
  let formatter = NumberFormatter()
  formatter.numberStyle = .currency
  formatter.currencyCode = "EUR"
  let value = Double(cents) / 100
    
 return formatter.string(from: NSNumber(value: value))!
}

func convertAmountToString(_ amount: Int) -> String {
    let formatter = NumberFormatter()
    formatter.numberStyle = .currency
    formatter.currencyCode = "EUR"
    formatter.maximumFractionDigits = 0
    
    return formatter.string(from: NSNumber(value:amount))!
}

func DistanceToString(_ km: Double) -> String {
  return "\(km) km"
}

let MatchRiderColor = UIColor(red: 125/255.0, green: 190/255.0, blue: 127/255.0, alpha: 1)
let MatchRiderThemeColor = "7DBE7F"

extension UITableViewCell {
  func enable(_ on: Bool) {
    for view in contentView.subviews {
      view.isUserInteractionEnabled = on
      view.alpha = on ? 1 : 0.5
    }
  }
}
