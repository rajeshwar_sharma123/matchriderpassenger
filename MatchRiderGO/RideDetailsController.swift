
import UIKit

class RideDetailsController: DetailsController {
  
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        NotificationCenter.default.addObserver(self, selector: #selector(RideDetailsController.rideSpecificNotificationReceiver(_:)), name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
        getRideDetail(rideId: ride.id)
        navigationController?.isToolbarHidden = true
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "RideSpecificNotification"), object: nil)
    }
    func rideSpecificNotificationReceiver(_ notification: NSNotification){
       _ = self.navigationController?.popViewController(animated: true)
    }

  @IBAction func callDriver() {
    let number = ride.driver!.phone.replacingOccurrences(of: " ", with: "")
    guard let url = URL(string: "tel://\(number)") else {
      let controller = ActionCallImpossible()
      present(controller, animated: true, completion: nil)
      return
    }
    UIApplication.shared.openURL(url)
  }
    @IBAction func messageDriver(){
        navigateToMessageViewController(messageThreadId: ride.messageThreadId)
    }
    
    func getRideDetail(rideId: Int) {
        if ride.messageThreadId != nil{
            return
        }
        
        HttpRequest.sharedRequest.getRideDetails(Store.sharedStore.token!, driverRequestId: rideId) { ride in
            guard let ride = ride else {
                return
            }
            print(ride)
            if ride.messageThreadId != nil{
            self.ride.messageThreadId = ride.messageThreadId
            }
        }
    }
    
    func getMessageThreadId(){
        self.view.showLoader()
        let driverUserId: [String] = [ride.messageThreadId!]

        HttpRequest.sharedRequest.getMessageThreadId(Store.sharedStore.token!, receiverPersonIds: driverUserId) { (messageThreadId) in
            guard let messageThreadId = messageThreadId else {
                self.view.hideLoader()
            return
            }
            self.view.hideLoader()
            if messageThreadId == ""{
                self.navigateToMessageViewController(messageThreadId:nil)
            }else{
              self.navigateToMessageViewController(messageThreadId:messageThreadId)
            }
            
            print(messageThreadId)
        }
    }
    
    func navigateToMessageViewController(messageThreadId: String?){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        controller.ride = ride;
        controller.showDriverDetail = true
        controller.messageThreadId = messageThreadId
        self.navigationController?.pushViewController(controller, animated: true)
    }
  @IBAction func cancelRide() {
    let controller = ActionControllerCancelRide {
      guard let token = Store.sharedStore.token else {
        return
      }
       self.view.showLoader()
      HttpRequest.sharedRequest.cancelRide(token, rideId: self.ride.id) { success, error in
        self.view.hideLoader()
        if success{
       _ = self.navigationController?.popViewController(animated: true)
        }
        else{
           let controller = ActionControllerCommonAlert(error)
           self.present(controller, animated: true, completion: nil)
        }
      }
    }
    present(controller, animated: true, completion: nil)
  }
  
}
